<?php

namespace Boydreams\NovaPermissions;

use App\Permission;
use Laravel\Nova\Contracts\ListableField;
use Laravel\Nova\Fields\Deletable;
use Laravel\Nova\Fields\DetachesPivotModels;
use Laravel\Nova\Fields\Field;
use Laravel\Nova\Fields\FormatsRelatableDisplayValues;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Contracts\Deletable as DeletableContract;

class Checkboxes extends Field
{
    use Deletable, DetachesPivotModels, FormatsRelatableDisplayValues;

    public function __construct($name, $attribute = null, $resource = null)
    {
        parent::__construct($name, $attribute);

        $this->fieldsCallback = function () {
            return [];
        };

    }

    /**
     * The callback that should be used to resolve the pivot fields.
     *
     * @var callable
     */
    public $fieldsCallback;

    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'FieldCheckboxes';

    /**
     * Specify the available options
     *
     * @param  array $options
     * @return self
     */
    public function options(array $options)
    {
        return $this->withMeta(['options' => $options]);
    }

    /**
     * Specify the callback to be executed to retrieve the pivot fields.
     *
     * @param  callable $callback
     * @return $this
     */
    public function fields($callback)
    {
        $this->fieldsCallback = $callback;

        return $this;
    }

    /**
     * Disable type casting of array keys to numeric values to return the unmodified keys.
     */
    public function withGroups()
    {
        return $this->withMeta(['withGroups' => true]);
    }

    /**
     * Hydrate the given attribute on the model based on the incoming request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest $request
     * @param  string $requestAttribute
     * @param  object $model
     * @param  string $attribute
     * @return void
     */
    protected function fillAttributeFromRequest(NovaRequest $request, $requestAttribute, $model, $attribute)
    {
        if ($request->exists($requestAttribute)) {
            /**
             * When editing entries, they are returned as comma seperated string (unsure why).
             * As a result we need to include this check and explode the values if required.
             */
            if (!is_array($choices = $request[$requestAttribute])) {
                $permissions = collect(explode(',', $choices))->reject(function ($name) {
                    return empty($name);
                })->all();
            }
            $speakers = Permission::whereIn('name',$permissions)->pluck('id')->toArray();

            //  $speakers = (array)$permissions; // related ids
            $pivotData = array_fill(0, count($speakers),
                ['clinic_id' => auth()->user()->clinic_id, 'user_id' => auth()->id()]);
            $syncData = array_combine($speakers, $pivotData);
            $model->Permissions()->sync($syncData);
            // $model->syncPermissions($syncData);
        }
    }
}
