<?php
namespace Boydreams\NovaPermissions\Nova;

use Laravel\Nova\Nova;
use App\Nova\Resource;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Illuminate\Validation\Rule;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\MorphToMany;
use Laravel\Nova\Fields\BelongsToMany;
use Boydreams\NovaPermissions\Nova\Role;
use App\Permission as SpatiePermission;

class Permission extends Resource
{
    /**
     * @var mixed
     */
    public static $displayInNavigation = false;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = SpatiePermission::class;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
    ];

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function fields(Request $request)
    {
        $guardOptions = collect(config('auth.guards'))->mapWithKeys(function ($value, $key) {
            return [$key => $key];
        });

        $userResource = Nova::resourceForModel(getModelForGuard($this->guard_name));

        return [
            ID::make('Id', 'id')
                ->rules('required')
                ->hideFromIndex()
            ,
            Text::make(__('Name'), 'name')
                ->rules(['required', 'string', 'max:255'])
                ->creationRules('unique:' . config('permission.table_names.permissions'))
                ->updateRules('unique:' . config('permission.table_names.permissions') . ',name,{{resourceId}}'),

            Text::make(__('Group'), 'group'),

            Select::make(__('Guard Name'), 'guard_name')
                ->options($guardOptions->toArray())
                ->rules(['required', Rule::in($guardOptions)]),

            // DateTime::make(__('nova-permission-tool::permissions.created_at'), 'created_at')->exceptOnForms(),
            // DateTime::make(__('nova-permission-tool::permissions.updated_at'), 'updated_at')->exceptOnForms(),

            BelongsToMany::make(__('Roles'), 'roles', Role::class)->fields(function () {
                return [

                    Text::make('', 'clinic_id')
                        ->withMeta([
                            'type' => 'hidden',
                            'value' => auth()->user()->clinic_id
                        ]),
                    Text::make('', 'user_id')
                        ->withMeta([
                            'type' => 'hidden',
                            'value' => auth()->id()
                        ]),
                ];
            }),
            MorphToMany::make($userResource::label(), 'users', $userResource)
                ->fields(function () {
                    return [

                        Text::make('', 'clinic_id')
                            ->withMeta([
                                'type' => 'hidden',
                                'value' => auth()->user()->clinic_id
                            ]),
                        Text::make('', 'user_id')
                            ->withMeta([
                                'type' => 'hidden',
                                'value' => auth()->id()
                            ]),
                    ];
                })
                ->searchable(),
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    public static function getModel()
    {
        //return app(PermissionRegistrar::class)->getPermissionClass();
    }

    public static function label()
    {
        return __('Permissions');
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    public static function singularLabel()
    {
        return __('Permission');
    }
}
